
# Create a Nexus Docker Repository
In this project we will create a Docker repository in Nexus Artifact Repository Manager and push/pull a docker image to/from it.

## Technologies Used
- Docker
- Nexus Artifact Repository Manager
- Node.js
- MongoDB
- Mongo-Express
- DigitalOcean Droplet
- Git
- Linux (Ubuntu)


## Project Description
- Create Docker hosted repository on Nexus
- Create Docker repository role on Nexus
- Configure Nexus, a DigitalOcean Droplet and Docker to be able to push to the Docker repository
- Build and Push Docker image to Docker repository on Nexus

## Prerequisites
- Digital Ocean account
- A droplet with Nexus Installed/Configured
- Linux machine with Git, Node.js, access configured for remote Nexus server

## Guide Steps
### Create a Nexus Docker Repository
- Connect to your Droplet hosted Nexus Repository via your web browser. Mine is running at `DROPLET_IP:8081`.
- Login to the Admin area
- Repositories > Create Repository > **Docker (hosted)**
	- Name: Docker-hosted
	- Blob Store: mystore (You can use whichever you created)
	- Allow **HTTP** and set port to `8083`
	- **Create Repository**

### Create an Access Role for the Docker Repository
- Security > Roles > **Create Role**
	- Type: Nexus Role
	- Role ID: nx-docker
	- Role Name: nx-docker
	- Role Description: For use with Docker Images Repository
	- Privilege: nx-repository-view-docker-docker-hosted-*
	- **Save**
- Security > Users > **Your Account**
	- Assign `nx-docker` role
	- **Save**

### Enable Docker Bearer Token Realm in Nexus
We need to enable Docker tokens for authentication inside of Nexus to allow `docker login`.
- Security > Realms
- Add `Docker Bearer Token Realm`
- **Save**

### Open Firewall Ports
- Since we configured HTTP for port 8083, we need to open it so we can access it remotely.
- From your DigitalOcean portal, navigate to Networking > Firewalls > **Your Firewall** or **Create Firewall**
- New Rule > Custom
	- Protocol: TCP
	- Port: 8083
	- Source: `YOUR_IP`
	- **Save**

### Configure (Insecure) HTTP for Docker Login
From your Linux machine we need to allow Docker to utilize our HTTP Nexus Repo. [We will add it as an insecure registry to our daemon.json file](https://docs.docker.com/registry/insecure/).
- `sudo vim /etc/docker/daemon.json`
````
{
  "insecure-registries" : ["NEXUS_REPOSITORY_IP.com:8083"]
}
````
- Restart the Docker service
	- `sudo systemctl restart docker`

### Docker Login to Confirm Setup
- `docker Login NEXUS_IP:8083`
	- You should be prompted for a username and password for the user we configured in Nexus
	- Login should be successful and it will cache a login token that will be used for future logins and pushing images.

### Build & Tag the Docker Image
We'll use the included project `my-app`
- Navigate to the project directory
	- `cd ~/project/my-app`
- `docker build -t my-app:1.0 .`
- `docker tag my-app:1.0 NEXUS_REPO_IP:8083:/my-app:1.0`

### Push Docker Image
- `docker push NEXUS_REPO_IP:8083:/my-app:1.0`

![Successful Docker Push](/images/m7-6-successful-docker-push.png)

### Fetch the Docker Image Information from Nexus
- Find the image from the repository
	- `curl -u NEXUS_USER:NEXUS_PASSWORD -X GET 'http://NEXUS_SERVER_IP:8081/service/rest/v1/components?repository=docker-hosted`
- You can now use the download URL with a curl command to download the image from the repository

![Curl Output](/images/m7-6-curl-output.png)